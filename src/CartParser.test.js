import { readFileSync } from 'fs';
import CartParser from './CartParser';

// fixture path
const getSamplePath = filename => {
	return `${__dirname}/../samples/${filename}`
}

const readFile = path => {
	return readFileSync(path, 'utf-8', 'r');
}

let parser;
let csvCartPath;
let notValidCsvCartPath;
let cart;
beforeEach(() => {
	parser = new CartParser();
});

beforeAll(() => {
	csvCartPath = getSamplePath('cart.csv');
	notValidCsvCartPath = getSamplePath('notValidCart.csv');
	cart = JSON.parse(readFile(getSamplePath('cart.json')));
})

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	it('Total sum', () => {
		const { items } = cart;
		const expectedSum = items.reduce(
			(acc, cur) => acc + cur.price * cur.quantity,
			0
		);
		const actualSum = parser.calcTotal(items);
		expect(actualSum).toBe(expectedSum);
	})
	it('Should parse line', () => {
		const { items: expectedItems } = cart;
		const
			contents = readFile(csvCartPath),
			lines = contents.split(/\n/).filter(l => l).filter((l, i) => i > 0),
			items = lines.map(l => parser.parseLine(l));

		for (let i = 0; i < items.length; i++) {
			expect(items[i]).toHaveProperty('id')
			expect(items[i]).toHaveProperty('name', expectedItems[i].name)
			expect(items[i]).toHaveProperty('price', expectedItems[i].price)
			expect(items[i]).toHaveProperty('quantity', expectedItems[i].quantity)
		}
	})
	it('Should read files', () => {
		const expectedData = readFile(csvCartPath);
		const actualData = parser.readFile(csvCartPath);
		expect(actualData).toBe(expectedData);
	})
	it('Should find validation errors', () => {
		const contents = readFile(notValidCsvCartPath);

		const validationErrors = parser.validate(contents);

		expect(validationErrors).toHaveLength(4);
	})
	it('Should return empty array if content if valid', () => {
		const contents = readFile(csvCartPath);

		const validationErrors = parser.validate(contents);

		expect(validationErrors).toHaveLength(0);
	})
	it('Should parse csv to json', () => {
		const { items: expectedItems } = cart;

		const actualCart = parser.parse(csvCartPath);
		const { items, total } = actualCart;

		expect(total).toBe(cart.total);
		for (let i = 0; i < items.length; i++) {
			expect(items[i]).toHaveProperty('id')
			expect(items[i]).toHaveProperty('name', expectedItems[i].name)
			expect(items[i]).toHaveProperty('price', expectedItems[i].price)
			expect(items[i]).toHaveProperty('quantity', expectedItems[i].quantity)
		}
	})
	it('Should throw validation error', () => {
		const cb = () => parser.parse(notValidCsvCartPath);
		expect(cb).toThrow('Validation failed!');
	})
	it('Should have needed properties', () => {
		expect(parser).toHaveProperty('ColumnType');
		expect(parser).toHaveProperty('ErrorType');
		expect(parser).toHaveProperty('schema');
		expect(parser).toHaveProperty('parse');
	})
	it('Should create error', () => {
		const
			type = 'Validation error',
			row = 2,
			column = 1,
			message = 'Expected to be..';

		const error = parser.createError(type, row, column, message);
		expect(error).toEqual({ type, row, column, message });
	});
	it('Should return a JSON object with cart items and total price', () => {
		const actualCart = parser.parse(csvCartPath);
		expect(actualCart).toHaveProperty('items');
		expect(actualCart.items.length).toBeGreaterThan(0);
		expect(actualCart).toHaveProperty('total', cart.total);
	})
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	it('Should call methods when parse', () => {
		class MockParser extends CartParser {
			constructor() {
				super()
			}
			readFile = jest.fn(() => 'Product name,Price,Quantity\ntoy,12,12\ncar,10,10');
			validate = jest.fn(() => []);
			parseLine = jest.fn(() => { });
			calcTotal = jest.fn(() => 100)
		}
		const mockParser = new MockParser();
		mockParser.parse(csvCartPath);
		expect(mockParser.readFile).toHaveBeenCalledTimes(1);
		expect(mockParser.validate).toHaveBeenCalledTimes(1);
		expect(mockParser.parseLine).toHaveBeenCalledTimes(2);
		expect(mockParser.calcTotal).toHaveBeenCalledTimes(1);
	})
});